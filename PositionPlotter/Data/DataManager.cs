﻿using MightyLittleGeodesy.Positions;
using PositionPlotter.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace PositionPlotter.Data
{
    public static class DataManager
    {
        public static List<Placemark> GetPlacemarksFromStream(Stream stream)
        {
            List<Placemark> placemarks = new List<Placemark>();

            using (StreamReader reader = new StreamReader(stream))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();

                    SourceLine sourceLine = ParseSourceLine(line);
                    if (sourceLine == null)
                        continue;

                    WGS84Position wgsPos = GetWGS84Position(sourceLine.North, sourceLine.East);

                    Placemark placemark = new Placemark()
                    {
                        Name = sourceLine.Name,
                        Latitude = wgsPos.Latitude,
                        Longitude = wgsPos.Longitude,
                        Marker = GetMarkerFromType(sourceLine.Type)
                    };

                    placemarks.Add(placemark);
                }
            }

            return placemarks;
        }

        public static List<Placemark> GetPlacemarksFromString(string text)
        {
            List<Placemark> placemarks = new List<Placemark>();

            string[] swerefLines = text.Split(new char[] { '\n' });

            foreach (var line in swerefLines)
            {
                SourceLine sourceLine = ParseSourceLine(line);
                if (sourceLine == null)
                    continue;

                WGS84Position wgsPos = GetWGS84Position(sourceLine.North, sourceLine.East);

                Placemark placemark = new Placemark()
                {
                    Name = sourceLine.Name,
                    Latitude = wgsPos.Latitude,
                    Longitude = wgsPos.Longitude,
                    Marker = GetMarkerFromType(sourceLine.Type)
                };

                placemarks.Add(placemark);
            }

            return placemarks;
        }

        private static WGS84Position GetWGS84Position(double north, double east)
        {
            WGS84Position wgsPos = null;
            if (east <= 999999)
            {
                SWEREF99Position swePos = new SWEREF99Position(north, east);
                wgsPos = swePos.ToWGS84();
            }
            else if (east > 999999)
            {
                RT90Position rt90Pos = new RT90Position(north, east);
                wgsPos = rt90Pos.ToWGS84();
            }
            else
            {
                throw new Exception("Okänt format på koordinat.");
            }

            return wgsPos;
        }

        private static string GetMarkerFromType(string type)
        {
            if (type.ToLower().Equals("blue"))
            {
                return "blue-dot.png";
            }
            else
            {
                return "red-dot.png";
            }
        }

        private static SourceLine ParseSourceLine(string line)
        {
            string[] parts = line.Split(new char[] { '\t' });
            double n = 0.0;
            if (false == double.TryParse(parts[0], out n)) return null;
            double e = 0.0;
            if (false == double.TryParse(parts[1], out e)) return null;

            return new SourceLine()
            {
                North = n,
                East = e,
                Name = parts[2],
                Type = parts[3]
            };
        }
   }
}