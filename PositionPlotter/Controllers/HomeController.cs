﻿using PositionPlotter.Data;
using PositionPlotter.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PositionPlotter.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<Placemark> placemarks = TempData["Placemarks"] as List<Placemark>;

            if (placemarks != null)
            {
                ViewBag.Names = placemarks.Select(s => s.Name).ToArray();
                ViewBag.Latitudes = placemarks.Select(s => s.Latitude).ToArray();
                ViewBag.Longitudes = placemarks.Select(s => s.Longitude).ToArray();
                ViewBag.Markers = placemarks.Select(s => s.Marker).ToArray();
            }

            return View();
        }

        public ActionResult Data()
        {
            ViewBag.Message = "Data upload page";

            return View();
        }

        [HttpPost]
        public ActionResult Data(FormCollection formData)
        {
            if (Request.Files != null && Request.Files[0] != null && Request.Files[0].ContentLength > 0)
            {
                TempData["Placemarks"] = DataManager.GetPlacemarksFromStream(Request.Files[0].InputStream);
                return RedirectToAction("Index");
            }
            else if (formData["text"] != null)
            {
                TempData["Placemarks"] = DataManager.GetPlacemarksFromString(formData["text"]);
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}