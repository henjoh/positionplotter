﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PositionPlotter.Models
{
    public class SourceLine
    {
        public double North { get; set; }
        public double East { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}